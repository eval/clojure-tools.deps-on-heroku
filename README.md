# Clojure tools.deps on Heroku

A minimal tools.deps Clojure project that can be deployed to Heroku.

For details see [this article](https://www.gertgoet.com/2019/02/06/deploying-a-tools-deps-clojure-project-to-heroku.html).

## deploying


Required:
- the [Heroku toolbelt](https://devcenter.heroku.com/articles/heroku-cli#download-and-install)
- a working copy of this repository

The following steps deploy the application:
```bash
$ cd $PROJECT
$ heroku apps:create <name-of-app>
$ git push heroku master
```

Now `heroku logs -t` should show the application started successfully:
```
2019-02-06T17:13:34.247102+00:00 app[api]: Initial release by user user@example.org                                                                       
2019-02-06T17:13:34.339628+00:00 app[api]: Enable Logplex by user user@example.org                                                                        
2019-02-06T17:13:34.247102+00:00 app[api]: Release v1 created by user user@example.org                                                                    
2019-02-06T17:14:38.705142+00:00 app[api]: Deploy 47f6fa2e by user user@example.org                                                                       
2019-02-06T17:15:04.937567+00:00 app[web.1]: :tick
2019-02-06T17:15:07.940100+00:00 app[web.1]: :tick
...
```